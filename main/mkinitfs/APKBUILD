# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=mkinitfs
pkgver=3.8.1
# shellcheck disable=SC2034 # used for git versions, keep around for next time
_ver=${pkgver%_git*}
pkgrel=2
pkgdesc="Tool to generate initramfs images for Alpine"
url="https://gitlab.alpinelinux.org/alpine/mkinitfs"
arch="all"
license="GPL-2.0-only"
# currently we do not ship any testsuite
options="!check"
makedepends_host="busybox kmod-dev util-linux-dev cryptsetup-dev linux-headers"
makedepends="$makedepends_host"
depends="
	apk-tools>=2.9.1
	busybox-binsh
	busybox>=1.28.2-r1
	kmod
	lddtree>=1.25
	mdev-conf
	"
subpackages="$pkgname-doc"
install="$pkgname.pre-upgrade $pkgname.post-install $pkgname.post-upgrade"
triggers="$pkgname.trigger=/usr/share/kernel/*"
source="https://gitlab.alpinelinux.org/alpine/mkinitfs/-/archive/$pkgver/mkinitfs-$pkgver.tar.gz
	zfs-2.2.patch"

provides="initramfs-generator"
provider_priority=900 # highest

build() {
	make VERSION=$pkgver-r$pkgrel
}

package() {
	make install DESTDIR="$pkgdir"
}

sha512sums="
f30398875f07c2dafc98ff6bbf82e8414c0f275b7ee7e6b6ae485c482b4f9f01175916b73c853df7a5528164e7f9cca574c7a67a668bf41cf74314dc3111a857  mkinitfs-3.8.1.tar.gz
3d47d5d22a60f5265ff9956fe6a9919411cd273b3a772b814cba1909f01aae244ae4050ef80c9224f10e1f24cf406aa4bf443b55bba1d6aecdf79c3948f9debf  zfs-2.2.patch
"
