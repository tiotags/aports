# Maintainer: Jeff Dickey <alpine@rtx.pub>
pkgname=rtx
pkgver=2023.11.9
pkgrel=0
pkgdesc="Polyglot runtime and dev tool version manager"
url="https://rtx.pub"
arch="all !s390x !riscv64 !ppc64le" # limited by cargo
license="MIT"
makedepends="cargo bash direnv cargo-auditable openssl-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/jdxcode/rtx/archive/refs/tags/v$pkgver.tar.gz"

prepare() {
	default_prepare
	git init .
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen --bin rtx
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/rtx \
		-t "$pkgdir"/usr/bin/
}

sha512sums="
ae3d5aa26b170a9a92fc3f05188dc3bc5cb4374032495918d1121b6f6d89a41b88b37e2b053a38d999286b5a5f56fc585e49905fadd239d012a796d9c13faff9  rtx-2023.11.9.tar.gz
"
