# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-trove-classifiers
pkgver=2023.11.22
pkgrel=0
pkgdesc="Canonical source for classifiers on PyPI"
url="https://github.com/pypa/trove-classifiers"
license="Apache-2.0"
arch="noarch"
depends="python3"
makedepends="py3-calver py3-gpep517 py3-installer py3-setuptools py3-wheel"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/pypa/trove-classifiers/archive/$pkgver/py3-trove-classifiers-$pkgver.tar.gz"
builddir="$srcdir/trove-classifiers-$pkgver"

prepare() {
	default_prepare

	echo "Version: $pkgver" > PKG-INFO
}

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/trove_classifiers-$pkgver-py3-none-any.whl
}

sha512sums="
d9e6898a088b642563c92e747675a70797db4033bae2acb4a52568271da02826541be5e89672e4f056262cc39bc5db245d49df39fdd7a57e2724dcafe9e36d30  py3-trove-classifiers-2023.11.22.tar.gz
"
